from flask import render_template, redirect, url_for, request
from flask import send_file
import os
from werkzeug.utils import secure_filename

from app import app
from app.forms import UploadFile
from .config import Config

import xlrd
import pandas as pd

def opus_to_excel(opus_file, first_data_row):
    '''
    Funcion para convertir un archivo de xls de OPUS en un archivo xlsx mejor estructurado
    para que cada fila corresponda a una partida.
    '''
    book = xlrd.open_workbook(opus_file)
    sh = book.sheet_by_index(0)
    partidas = []
    
    for row in range(first_data_row, sh.nrows):
        if sh.cell_value(rowx=row, colx = 5):
            partida = {}
            inerrow = row
            partida['Clave'] = ""
            while sh.cell_value(rowx=inerrow, colx = 0):
                partida['Clave'] += sh.cell_value(rowx=inerrow, colx = 0)
                inerrow += 1
            inerrow = row
            partida['Descripcion'] = ""
            while sh.cell_value(rowx=inerrow, colx = 1):
                partida['Descripcion'] += str(sh.cell_value(rowx=inerrow, colx = 1))
                inerrow += 1
            partida['Unidad'] = sh.cell_value(rowx=row, colx = 3)
            partida['Cantidad'] = sh.cell_value(rowx=row, colx = 4)
            partida['Costo_unitario'] = sh.cell_value(rowx=row, colx = 5)
            partida['Importe'] = sh.cell_value(rowx=row, colx = 6)
            partida['Porcentaje'] = sh.cell_value(rowx=row, colx = 8)
            partidas.append(partida)
    try:
        pd.DataFrame.from_dict(partidas).to_excel(
            os.path.splitext(opus_file)[0] + ".xlsx", index = False
        )
        return True
    except:
        return False
    
@app.route('/tools/opustoxlsx', methods=['GET', 'POST'])
def opustoxlsx():
    '''
    Formulario donde se envia el archivo y que automaticamente reponde con el nuevo archivo
    '''
    form = UploadFile()
    if form.validate_on_submit():
        opusfile = form.files.data
        filename = secure_filename(opusfile.filename)
        fullpath = os.path.join('app/upload', filename)
        print(os.path.splitext(fullpath))
        opusfile.save(fullpath)
        opus_to_excel(fullpath, 16)
        return send_file(
            os.path.join(
                'upload', os.path.split(os.path.splitext(fullpath)[0])[1]
            )
             + ".xlsx"
        )
    context = {
        'form':form
    }
    return render_template('upload.html', **context)