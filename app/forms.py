from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms.fields.simple import FileField, SubmitField

class UploadFile(FlaskForm):
    '''
    Form to upload single xls file from opus
    '''
    files = FileField('Archivo XLS de opus', validators=[FileAllowed(['xls'], 'Solo archivos xls')])
    submit = SubmitField('Subir archivo')
