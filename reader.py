import xlrd
import pandas as pd

def opus_to_excel(opus_file, first_data_row):
    book = xlrd.open_workbook(opus_file)
    sh = book.sheet_by_index(0)
    partidas = []
    
    for row in range(first_data_row, sh.nrows):
        if sh.cell_value(rowx=row, colx = 5):
            partida = {}
            inerrow = row
            partida['Clave'] = ""
            while sh.cell_value(rowx=inerrow, colx = 0):
                partida['Clave'] += sh.cell_value(rowx=inerrow, colx = 0)
                inerrow += 1
            inerrow = row
            partida['Descripcion'] = ""
            while sh.cell_value(rowx=inerrow, colx = 1):
                partida['Descripcion'] += str(sh.cell_value(rowx=inerrow, colx = 1))
                inerrow += 1
            partida['Unidad'] = sh.cell_value(rowx=row, colx = 3)
            partida['Cantidad'] = sh.cell_value(rowx=row, colx = 4)
            partida['Costo_unitario'] = sh.cell_value(rowx=row, colx = 5)
            partida['Importe'] = sh.cell_value(rowx=row, colx = 6)
            partida['Porcentaje'] = sh.cell_value(rowx=row, colx = 8)
            partidas.append(partida)
    try:
        pd.DataFrame.from_dict(partidas).to_excel("test2.xlsx", index = False)
        return True
    except:
        return False

print(opus_to_excel("test.xls", 16))